## Explicar de que se trata el curso
El curso de **GIT** tiene como propósito dar ***una introducción*** al uso de las herramientas de control de versiones, dar una vista general de distintos softwares, commandos importantes y uso correcto de los branchs y tags.

## Herramientas web para aprender ramas en GIT
* [git-scm](#item1)
* [GitHub Learning Lab ](#item2)

<a name="item1"></a>
###  https://git-scm.com/doc
 
**Descripción**: La documentación oficial de Git es un recurso valioso. Puedes encontrar información detallada sobre cómo trabajar con ramas, fusiones y otros conceptos de Git.

 
<a name="item2"></a>
### https://lab.github.com/
 
**Descripción**: GitHub Learning Lab ofrece varios cursos interactivos relacionados con Git y GitHub, incluyendo tutoriales sobre ramas, fusiones y colaboración en equipo.
Atlassian Bitbucket Tutorial:

En este repositorio podés descargar los archivos de Poncho para trabajar de manera local.
Esta nueva versión de Poncho incluye cambios de colores y otros elementos que mejoran cuestiones de accesibilidad.

![Poncho](img/poncho.gif)

# Poncho

Base de html y css para la creación de sitios pertenecientes a la Administración Pública Nacional de la República Argentina.



Para usar Poncho en un sitio, ver [la documentación](http://argob.github.io/poncho).  
También estamos en [NPM](https://www.npmjs.com/package/ar-poncho).

# Que es poncho para el trabajo de IPAP
Poncho es un proyecto de codigo abierto, para poder armar sitios esteticamente similares en el ambiente del gobierno nacional argentino.
Fin-agregado-por-curso IPAP


## ¿Cómo instalar Poncho?

#### Si usás NPM

* Ejecutá en la consola el comando **npm i ar-poncho**

#### Si descargás los archivos manualmente

* También podés [descargar las plantillas de Poncho](http://argob.github.io/poncho/plantillas/paginas-de-argentina/) o crear archivos html nuevos, asegurándote de que en los html estén declaradas estas dependencias:
  
#### Fuente Tipográfica - Encode

* ``` <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Encode+Sans:wght@100;200;300;400;500;600;700;800;900&amp;display=swap" media="all" /> ```

#### CSS - Bootstrap (v-3.4.1), Font Awesome (v-4.7.0), Poncho, Iconos

* ``` <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"  rel="stylesheet"> ```
* ``` <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> ```
* ``` <link rel="stylesheet" href="dist/css/poncho.min.css"> ```
* ``` <link rel="stylesheet" href="dist/css/icono-arg.css"> ```

#### JavaScript -  Jquery, Bootstrap (v-3.4.1)

* ``` <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> ```
* ``` <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js”></script> ```

